# Stagemarkt

> Timer voor de Howest Stagemarkt

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

```

## Belangrijk

De timer gebruikt de tijd en datum van het toestel waarop de webpagina geladen wordt. 
Zorg dus dat deze juist staan.

## Aanpassen

Om het begintijdstip aan te passen ga je na src/App.vue.
Het aantal millesecondes kan je berekenen via https://currentmillis.com/ 

``` javascript
let start = new Date(1575534900000)
```

De pauze wordt gedefinieerd via dit stukje code.

```
if (i === 5) {
    this.timeslots[i].name = 'pauze'
} else {
    nameCounter++
}
```

Succes!